//
// Created by korca on 03.01.2020.
//

#include "Stroj.h"


Stroj::Stroj(int pracDoba ,int cena, int pocet, int cenaUdrzby){
    m_pracDoba = pracDoba;
    m_cena = cena;
    m_pocetStrojov = pocet;
    m_pracovnaDobaVsech = pocet*pracDoba;
    m_cenaUdrzby = cenaUdrzby;

}

int Stroj::getPracDoba(){
    return m_pracDoba;
}

int Stroj::getPracDobaVsech(){
    // Násobeno počtem, protože se pracovní doba odvijí od počtu strojů.

    return m_pracovnaDobaVsech;
}

int Stroj::getCena(){
    return m_cena;
}

int Stroj::getPocet(){
    return m_pocetStrojov;
}
int Stroj::getCenaUdrzby(){
    return m_cenaUdrzby;
}

void Stroj::setPracDoba(int doba){
    m_pracovnaDobaVsech= doba;
}


void Stroj::setCena(int cena){
    m_cena = cena;
}

void Stroj::setPocet(int pocet){
    m_pocetStrojov = pocet;
}

void Stroj::getCenaUdrzby(int cena){
    m_pocetStrojov*cena;
}



void Stroj::printInfo(){
    std::cout << "Pocet stroju je: " << getPocet() << std::endl;
    std::cout << "Prac. doba jednoho je: " << getPracDoba() << std::endl;
    std::cout << "Prac. doba k dispozici je: "<<getPracDobaVsech()<< std::endl;

}