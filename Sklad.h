//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_SKLAD_H
#define UNTITLED9_SKLAD_H
#include <cstdlib>
#include <ctime>
#include "Vyrobek.h"
#include "Surivona.h"
#include "Stroj.h"

class Sklad {

    int m_pocetDreva;
    int m_pocetKamena;
    int m_pocetZeleza;
public:
    Sklad();
    //Zacatek surovin
    int getPocetDreva();
    int getPocetKamena();
    int getPocetZeleza();
    void pridejDrevo(int kolik);
    void pridejKamen(int kolik);
    void pridejZelezo(int kolik);
    void odoberDrevo(int kolik);
    void odoberKamen(int kolik);
    void odoberZelezo(int kolik);
    int getCenuDreva();
    int getCenuKamena();
    int getCenuZeleza();
    void setCenaDreva(int cena);
    void setCenaKamena(int cena);
    void setCenaZeleza(int cena);
    int getCenaUskladneni();
    //Konec surovin

    //Zacatek vyrobku
    int getPocetZidli();
    int getPocetStolu();
    int getPocetSkrin();
    int getPocetPolic();
    int getCenaZidle();
    int getCenaStolu();
    int getCenaPolice();
    int getCenaSkrine();
    int getCenaDrevaZidle();
    int getCenaKameneZidle();
    int getCenaZelezaZidle();
    int getCenaDrevaStul();
    int getCenaKameneStul();
    int getCenaZelezaStul();
    int getCenaDrevaPolice();
    int getCenaKamenePolice();
    int getCenaZelezaPolice();
    int getCenaDrevaSkrin();
    int getCenaKameneSkrin();
    int getCenaZelezaSkrin();
    int getCasVyrobyZidle();
    int getCasVyrobyStul();
    int getCasVyrobyPolice();
    int getCasVyrobySkrin();
    int getCenaStroja();
    int getPocetStroju();
    int getCenaUdrzby();
    int getPracovnyCas();
    void setPracDoba(int kolik);
    void pridajPracDobu(int kolik);
    void odoberPracDobu(int kolik);
    void pridejStroj(int pocet);
    void odeberStroj(int pocet);
    void pridajVyrobokZidle(int kolik);
    void pridajVyrobokStul(int kolik);
    void pridajVyrobokSkrin(int kolik);
    void pridajVyrobokPolice(int kolik);
    void odoberVyrobokZidle(int kolik);
    void odoberVyrobokStul(int kolik);
    void odoberVyrobokSkrin(int kolik);
    void odoberVyrobokPolice(int kolik);

    //Konec vyrobku

    void printInfo();
};



#endif //UNTITLED9_SKLAD_H
