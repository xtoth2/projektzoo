//
// Created by david on 17. 12. 2019.
//

#include "Surivona.h"
Surivona::Surivona(std::string nazev,int cena, int cenaUskladnenia){
    m_nazev = nazev;
    m_cena = cena;
    m_cenaUskladnenia = cenaUskladnenia;
    m_pocet = 0;
}

std::string Surivona::getNazev(){
    return m_nazev;
}
int Surivona::getCena(){
    return m_cena;
}
int Surivona::getcenaUskladnenia(){
    return m_cenaUskladnenia;
}
int Surivona::getPocet() {
    return m_pocet;
}
void Surivona::setPocet(int pocet) {
    m_pocet = pocet;
}
void Surivona::setCena(int cena){
    m_cena = cena;
}