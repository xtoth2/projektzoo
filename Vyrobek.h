//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_VYROBEK_H
#define UNTITLED9_VYROBEK_H

#include <iostream>



class Vyrobek {
    std::string m_nazev;
    int m_cena;
    int m_cenaDreva;
    int m_cenaKamene;
    int m_cenaZeleza;
    int   m_casVyroby;
    int m_pocet;
public:
    Vyrobek(std::string nazev,int cena, int cenaDreva, int cenaKamene, int cenaZeleza,int   m_casVyroby);
    std::string getNazev();
    int getCena();
    int getCenaDreva();
    int getCenaKamene();
    int getCenaZeleza();
    int getCasVyroby();
    int getPocet();
    void setPocet(int pocet);

};



#endif //UNTITLED9_VYROBEK_H
