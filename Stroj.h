//
// Created by korca on 03.01.2020.
//

#ifndef UNTITLED9_STROJ_H
#define UNTITLED9_STROJ_H

#include <iostream>

class Stroj {
    int m_pracDoba;
    int m_cena;
    int m_pocetStrojov;
    int m_pracovnaDobaVsech;
    int m_cenaUdrzby;

public:
    Stroj(int pracDoba ,int cena, int pocet, int cenaUdrzby);
    int getPracDoba();
    int getPracDobaVsech();
    int getCena();
    int getPocet();
    int getCenaUdrzby();
    void setPracDoba(int doba);

    void setCena(int cena);
    void setPocet(int pocet);
    void getCenaUdrzby(int cena);
    void printInfo();
};


#endif //UNTITLED9_STROJ_H
