//
// Created by david on 17. 12. 2019.
//

#include "Sklad.h"

Sklad::Sklad(){

}

//Zacatek surovin
Surivona* drevo = new Surivona("drevo",10,1);
Surivona* kamen = new Surivona("kamen",15,2);
Surivona* zelezo = new Surivona("zelezo",20,3);
void Sklad::setCenaDreva(int cena){
    drevo->setPocet(cena);
}
void Sklad::setCenaKamena(int cena){
    kamen->setCena(cena);
}
void Sklad::setCenaZeleza(int cena){
    zelezo->setCena(cena);
}
int Sklad::getPocetDreva(){
    m_pocetDreva = drevo->getPocet();
    return m_pocetDreva;
}

int Sklad::getPocetKamena(){
    m_pocetKamena = kamen ->getPocet();
    return  m_pocetKamena;
}

int Sklad::getPocetZeleza(){
    m_pocetZeleza = zelezo->getPocet();
    return m_pocetZeleza;
}

void Sklad::pridejDrevo(int kolik) {
    drevo->setPocet(kolik+drevo->getPocet());
}

int Sklad::getCenuDreva() {
    return drevo->getCena();
}

int Sklad::getCenuKamena(){
    return kamen->getCena();
}

int Sklad::getCenuZeleza(){
    return zelezo->getCena();
}

void Sklad::pridejKamen(int kolik){
    kamen->setPocet(kolik);
}

void Sklad::pridejZelezo(int kolik){
    zelezo->setPocet(kolik);
}

void Sklad::odoberDrevo(int kolik){
    if (kolik>drevo->getPocet()){
        std::cout<<"Nemas tolko dreva."<<std::endl;
    }
    else{
        drevo->setPocet(drevo->getPocet()-kolik);
    }
}

void Sklad::odoberKamen(int kolik){
    if (kolik>kamen->getPocet()){
        std::cout<<"Nemas tolko kamena."<<std::endl;
    }
    else{
        kamen->setPocet(kamen->getPocet()-kolik);
    }
}

void Sklad::odoberZelezo(int kolik){
    if (kolik>zelezo->getPocet()){
        std::cout<<"Nemas tolko zeleza."<<std::endl;
    }
    else{
        zelezo->setPocet(zelezo->getPocet()-kolik);
    }
}
int Sklad::getCenaUskladneni(){
    return drevo->getcenaUskladnenia()*drevo->getPocet()+kamen->getcenaUskladnenia()*kamen->getPocet()+zelezo->getcenaUskladnenia()*zelezo->getPocet();
}
//Konec surovin


//Zacatek vyrobku
Vyrobek* zidle = new Vyrobek("zidle",100,10,5,2,100);
Vyrobek* stul = new Vyrobek("stul",150,15,7,5,300);
Vyrobek* police = new Vyrobek("police",200,20,10,7,500);
Vyrobek* skrin = new Vyrobek("skrin",300,30,20,15,700);

int Sklad::getPocetZidli(){
    return zidle->getPocet();
}

int  Sklad::getPocetStolu(){
    return stul->getPocet();
}

int  Sklad::getPocetSkrin(){
    return skrin->getPocet();
}

int  Sklad::getPocetPolic(){
    return police->getPocet();
}
//Zacatek vyroby
Stroj* stroj = new Stroj(1000, 5000, 3,50);
void  Sklad::pridajVyrobokZidle(int kolik){
    if (getPocetDreva()<zidle->getCenaDreva()*kolik or stroj->getPracDobaVsech()<zidle->getCasVyroby()*kolik or getPocetZeleza()<zidle->getCenaZeleza()*kolik or getPocetKamena()<zidle->getCenaKamene()*kolik){
        std::cout<< "Nemas dostatok materialu, alebo strojoveho casu."<<std::endl;
    }else {
        stroj->setPracDoba(stroj->getPracDobaVsech()-zidle->getCasVyroby()*kolik);
        zidle->setPocet(zidle->getPocet() + kolik);
        drevo->setPocet(drevo->getPocet()-zidle->getCenaDreva()*kolik);
        kamen->setPocet(kamen->getPocet()-zidle->getCenaKamene()*kolik);
        zelezo->setPocet(zelezo->getPocet()-zidle->getCenaZeleza()*kolik);
    }

}

void  Sklad::pridajVyrobokStul(int kolik){
    if (getPocetDreva()<stul->getCenaDreva()*kolik or stroj->getPracDobaVsech()<stul->getCasVyroby()*kolik or getPocetZeleza()<stul->getCenaZeleza()*kolik or getPocetKamena()<stul->getCenaKamene()*kolik){
        std::cout<< "Nemas dostatok materialu, alebo strojoveho casu."<<std::endl;
    }
    else {
        stroj->setPracDoba(stroj->getPracDobaVsech()-stul->getCasVyroby()*kolik);
        stul->setPocet(stul->getPocet() + kolik);
        drevo->setPocet(drevo->getPocet()-stul->getCenaDreva()*kolik);
        kamen->setPocet(kamen->getPocet()-stul->getCenaKamene()*kolik);
        zelezo->setPocet(zelezo->getPocet()-stul->getCenaZeleza()*kolik);
    }
}

void  Sklad::pridajVyrobokSkrin(int kolik){
    if (getPocetDreva()<skrin->getCenaDreva()*kolik or stroj->getPracDobaVsech()<police->getCasVyroby()*kolik or getPocetZeleza()<skrin->getCenaZeleza()*kolik or getPocetKamena()<skrin->getCenaKamene()*kolik){
        std::cout<< "Nemas dostatok materialu, alebo strojoveho casu."<<std::endl;
    }else {
        stroj->setPracDoba(stroj->getPracDobaVsech()-skrin->getCasVyroby()*kolik);
        skrin->setPocet(skrin->getPocet() + kolik);
        drevo->setPocet(drevo->getPocet()-skrin->getCenaDreva()*kolik);
        kamen->setPocet(kamen->getPocet()-skrin->getCenaKamene()*kolik);
        zelezo->setPocet(zelezo->getPocet()-skrin->getCenaZeleza()*kolik);
    }

}

void  Sklad::pridajVyrobokPolice(int kolik){
    if (getPocetDreva()<police->getCenaDreva()*kolik or stroj->getPracDobaVsech()<police->getCasVyroby()*kolik or getPocetZeleza()<police->getCenaZeleza()*kolik or getPocetKamena()<police->getCenaKamene()*kolik){
        std::cout<< "Nemas dostatok materialu, alebo strojoveho casu."<<std::endl;
    }else {
        stroj->setPracDoba(stroj->getPracDobaVsech()-police->getCasVyroby()*kolik);
        police->setPocet(police->getPocet() + kolik);
        drevo->setPocet(drevo->getPocet()-police->getCenaDreva()*kolik);
        kamen->setPocet(kamen->getPocet()-police->getCenaKamene()*kolik);
        zelezo->setPocet(zelezo->getPocet()-police->getCenaZeleza()*kolik);


    }
}
void Sklad::setPracDoba(int kolik){
    stroj->setPracDoba(kolik);
}
int Sklad::getPracovnyCas(){
    return stroj->getPracDoba();
}
int Sklad::getCenaStroja(){
    return stroj->getCena();
}
void Sklad::pridejStroj(int pocet){
    stroj->setPocet(stroj->getPocet()+pocet);
}

void Sklad::odeberStroj(int pocet){
    stroj->setPocet(stroj->getPocet()-pocet);
}
int Sklad::getPocetStroju(){
    return stroj->getPocet();
}
int Sklad::getCenaUdrzby(){
    return stroj->getCenaUdrzby()*stroj->getPocet();
}
void Sklad::pridajPracDobu(int kolik){
    stroj->setPracDoba(stroj->getPracDobaVsech()+stroj->getPracDoba()*kolik);
}
void Sklad::odoberPracDobu(int kolik){
    stroj->setPracDoba(stroj->getPracDobaVsech()-stroj->getPracDoba()*kolik);
}
void  Sklad::odoberVyrobokZidle(int kolik){
    if(getPocetZidli()<kolik){
        std::cout<<"Nemas tolko zidli."<<std::endl;
    }
    else{
        zidle->setPocet(zidle->getPocet()-kolik);
    }
}

void  Sklad::odoberVyrobokStul(int kolik){
    if(getPocetStolu()<kolik){
        std::cout<<"Nemas tolko stolu."<<std::endl;
    }
    else{
        stul->setPocet(stul->getPocet()-kolik);
    }
}

void  Sklad::odoberVyrobokSkrin(int kolik){
    if(getPocetSkrin()<kolik){
        std::cout<<"Nemas tolko skrin."<<std::endl;
    }
    else{
        skrin->setPocet(skrin->getPocet()-kolik);
    }
}

void  Sklad::odoberVyrobokPolice(int kolik){
    if(getPocetPolic()<kolik){
        std::cout<<"Nemas tolko polic."<<std::endl;
    }
    else{
        police->setPocet(police->getPocet()-kolik);
    }
}

int Sklad::getCenaZidle(){
    return zidle->getCena();
}

int Sklad::getCenaStolu(){
    return stul->getCena();
}

int Sklad::getCenaPolice(){
    return police->getCena();
}

int Sklad::getCenaSkrine(){
    return skrin->getCena();
}
int Sklad::getCenaDrevaZidle(){
    return zidle->getCenaDreva();
}
int Sklad::getCenaKameneZidle(){
    return zidle->getCenaKamene();
}
int Sklad::getCenaZelezaZidle(){
    return zidle->getCenaZeleza();
}
int Sklad::getCenaDrevaStul(){
    return stul->getCenaDreva();
}
int Sklad::getCenaKameneStul(){
    return stul->getCenaKamene();
}
int Sklad::getCenaZelezaStul(){
    return stul->getCenaZeleza();
}
int Sklad::getCenaDrevaPolice(){
    return police->getCenaDreva();
}
int Sklad::getCenaKamenePolice(){
    return police->getCenaKamene();
}
int Sklad::getCenaZelezaPolice(){
    return police->getCenaZeleza();
}
int Sklad::getCenaDrevaSkrin(){
    return skrin->getCenaDreva();
}
int Sklad::getCenaKameneSkrin(){
    return skrin->getCenaKamene();
}
int Sklad::getCenaZelezaSkrin(){
    return skrin->getCenaZeleza();
}
int Sklad::getCasVyrobyZidle(){
    return zidle->getCasVyroby();
}
int Sklad::getCasVyrobyStul(){
    return stul->getCasVyroby();
}
int Sklad::getCasVyrobyPolice(){
    return police->getCasVyroby();
}
int Sklad::getCasVyrobySkrin(){
    return skrin->getCasVyroby();
}
//Konec vyrobku

void Sklad::printInfo() {
    std::cout << "Pocet dreva je: " << getPocetDreva() << std::endl;
    std::cout << "Pocet kamene je: " << getPocetKamena() << std::endl;
    std::cout << "Pocet zeleza je: " << getPocetZeleza() << std::endl;
    std::cout << std::endl;
    std::cout << "Pocet zidli je: " << getPocetZidli() << std::endl;
    std::cout << "Pocet stolu je: " << getPocetStolu() << std::endl;
    std::cout << "Pocet skrin je: " << getPocetSkrin() << std::endl;
    std::cout << "Pocet polic je: " << getPocetPolic() << std::endl;
    stroj->printInfo();

}
