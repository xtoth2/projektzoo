//
// Created by david on 17. 12. 2019.
//

#include "Vyrobek.h"
Vyrobek::Vyrobek(std::string nazev, int cena, int cenaDreva, int cenaKamene, int cenaZeleza, int casVyorby){
    m_nazev = nazev;
    m_cena = cena;
    m_cenaDreva = cenaDreva;
    m_cenaKamene = cenaKamene;
    m_cenaZeleza = cenaZeleza;
    m_casVyroby = casVyorby;
    m_pocet = 0;
}
int Vyrobek::getCasVyroby(){
    return m_casVyroby;
}
std::string Vyrobek::getNazev(){
    return m_nazev;
}
int Vyrobek::getCena(){
    return m_cena;
}
int Vyrobek::getCenaDreva(){
    return m_cenaDreva;
}
int Vyrobek::getCenaKamene(){
    return m_cenaKamene;
}
int Vyrobek::getCenaZeleza(){
    return m_cenaZeleza;
}
int Vyrobek::getPocet() {
    return m_pocet;
}
void Vyrobek::setPocet(int pocet){
    m_pocet = pocet;
}

