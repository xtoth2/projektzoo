//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_HRA_H
#define UNTITLED9_HRA_H

#include "Sklad.h"
#include "Obchod.h"
#include "Stroj.h"

class Hra {
public:
    Hra();
    void inicializacia();
    void prubehHry();
    void vybermoznostiNakupu();
    void vybermoznostiProdeje();
    void prodejVyrobku();
    void vyrobaVyrobku();
};

#endif //UNTITLED9_HRA_H
