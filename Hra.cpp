//
// Created by david on 17. 12. 2019.
//

#include "Hra.h"


int mnozstvo,y,x;
int pocetKol=1;
Sklad* sklad = new Sklad();
Obchod* obchod = new Obchod();



Hra::Hra() {
}
void Hra::inicializacia() {
    std::cout<<"Zacina nasa hra, toto mas na zaciatok:" <<std::endl;
    obchod->printInfo();
    std::cout<<std::endl;
    sklad->printInfo();
    std::cout<<std::endl;


    std::cout<<std::endl;
}
void Hra::prubehHry() {
    std::cout<<"Kolo cislo: "<<pocetKol<<std::endl;
    std::cout<< "Zadej 1 pro nakup surovin."<<std::endl;
    std::cout<< "Zadej 2 pro prodej surovin."<<std::endl;
    std::cout<< "Zadej 3 pro prodej vyrobku."<<std::endl;
    std::cout<< "Zadej 4 pro vyrobu vyrobku."<<std::endl;
    std::cout<< "Zadej 5 pro nakup/prodej stroju."<<std::endl;
    std::cout<< "Zadej 99 pro ukonceni kola."<<std::endl;
    std::cin>>x;

    switch (x){
        case 1: {
            vybermoznostiNakupu();

        }
        case 2:{
            vybermoznostiProdeje();

        }
        case 3:{
            prodejVyrobku();
        }
        case 4:{
            vyrobaVyrobku();
        }
        case 5:{
            int p;
            std::cout<<"Pro nakup stroju zadej 1."<<std::endl;
            std::cout<<"Pro prodej stroju zadej 2."<<std::endl;
            std::cout<<"Pro navrat zadej 0."<<std::endl;
            std::cin>>p;
            switch (p){
                case 0:{
                    prubehHry();
                }
                case 1:{
                    std::cout<<"Jeden stroj stoji: " << sklad->getCenaStroja()<<std::endl;
                    std::cout<<"Zadej kolik stroju chces koupit: "<<std::endl;
                    int kolik;
                    std::cin>>kolik;
                    if (obchod->getFinance()<sklad->getCenaStroja()*kolik){
                        std::cout<<"Nemas dostatek finacii."<<std::endl;
                        obchod->printInfo();
                        sklad->printInfo();
                        prubehHry();
                    } else{
                        sklad->pridejStroj(kolik);
                        obchod->setFinance(obchod->getFinance()-sklad->getCenaStroja()*kolik);
                        sklad->pridajPracDobu(kolik);
                        obchod->printInfo();
                        sklad->printInfo();
                        prubehHry();
                    }
                }
                case 2:{
                    std::cout<<"Jeden stroj muzes prodat za: "<<sklad->getCenaStroja()/2<<std::endl;
                    std::cout<<"Zadej kolik stroju chces prodat: "<<std::endl;
                    int kolik;
                    std::cin>>kolik;
                    if(sklad->getPocetStroju()<kolik){
                        std::cout<<"Nemas tolik stroju."<<std::endl;
                        obchod->printInfo();
                        sklad->printInfo();
                        prubehHry();
                    } else{
                        sklad->odeberStroj(kolik);
                        obchod->setFinance(obchod->getFinance()+(sklad->getCenaStroja()*kolik)/2);
                        sklad->odoberPracDobu(kolik);
                        obchod->printInfo();
                        sklad->printInfo();
                        prubehHry();
                    }
                }

            }

        }
        case 99:{
            std::cout<<"Po ukonceni kola musis zaplatit poplatek za uskladneni materialu a za udrzbu stroju."<<std::endl;
            std::cout<<"Poplatek za uskladneni materialu je: " << sklad->getCenaUskladneni()<<std::endl;
            std::cout<<"Poplatek za udrzbu stroju je: "<<sklad->getCenaUdrzby()<<std::endl;
            std::cout<<"Jestli chces opravdu skoncit kolo zadej 1, pokud ne, zadej 0."<<std::endl;
            int cislo;
            std::cin>>cislo;
            switch (cislo){
                case 0:{
                   prubehHry();
                }
                case 1:{
                    if(obchod->getFinance()<(sklad->getCenaUdrzby()+sklad->getCenaUskladneni())){
                        std::cout<<"Nemas dostatok financii na ukonceni kola."<<std::endl;
                        prubehHry();
                    }
                    else{
                        obchod->setFinance(obchod->getFinance()-(sklad->getCenaUdrzby()+sklad->getCenaUskladneni()));
                        sklad->setPracDoba(sklad->getPocetStroju()*sklad->getPracovnyCas());
                        pocetKol++;
                        obchod->printInfo();
                        sklad->printInfo();
                        prubehHry();

                    }
                }
            }
        }
    }

}


void Hra::vybermoznostiNakupu() {
    std::cout<<"Pro nakup dreva zadej 1."<<std::endl;
    std::cout<<"Pro nakup kamene zadej 2."<<std::endl;
    std::cout<<"Pro nakup zeleza zadej 3."<<std::endl;
    std::cout<<"Pro navrat zadej 4."<<std::endl;
    std::cin>>y;

    switch (y){
        case 1:{

            std::cout<<"Drevo ma cenu " << sklad->getCenuDreva()<<std::endl;
            std::cout<<"Zadaj mnozstvo, ktore chces kupit: "<<std::endl;
            std::cout<<"Ak sa chces vratit zadaj: 0 "<<std::endl;
            std::cin>>mnozstvo;
            if (sklad->getCenuDreva()*mnozstvo > obchod->getFinance()){
                std::cout<<"Nemas dostatok financii." <<std::endl;
                vybermoznostiNakupu();
            }
            else{
                obchod->setFinance(obchod->getFinance()-(mnozstvo*sklad->getCenuDreva()));
                sklad->pridejDrevo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiNakupu();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();

        }
        case 2:{
            std::cout<<"Kamen ma cenu " << sklad->getCenuKamena()<<std::endl;
            std::cout<<"Zadaj mnozstvo, ktore chces kupit: "<<std::endl;
            std::cout<<"Ak sa chces vratit zadaj: 0 "<<std::endl;
            std::cin>>mnozstvo;
            if (sklad->getCenuKamena()*mnozstvo > obchod->getFinance()){
                std::cout<<"Nemas dostatok financii." <<std::endl;
                vybermoznostiNakupu();
            }
            else{
                obchod->setFinance(obchod->getFinance()-(mnozstvo*sklad->getCenuKamena()));
                sklad->pridejKamen(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiNakupu();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 3:{
            std::cout<<"Zelezo ma cenu " << sklad->getCenuZeleza()<<std::endl;
            std::cout<<"Zadaj mnozstvo, ktore chces kupit: "<<std::endl;
            std::cout<<"Ak sa chces vratit zadaj: 0 "<<std::endl;
            std::cin>>mnozstvo;
            if (sklad->getCenuZeleza()*mnozstvo > obchod->getFinance()){
                std::cout<<"Nemas dostatok financii." <<std::endl;
                vybermoznostiNakupu();
            }
            else{
                obchod->setFinance(obchod->getFinance()-(mnozstvo*sklad->getCenuZeleza()));
                sklad->pridejZelezo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiNakupu();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 4:{
            prubehHry();
        }

    }
}
void Hra::vybermoznostiProdeje() {
    std::cout << "Pro prodej dreva zadej 1." << std::endl;
    std::cout << "Pro prodej kamene zadej 2." << std::endl;
    std::cout << "Pro prodej zeleza zadej 3." << std::endl;
    std::cout << "Pro navrat zadej 4." << std::endl;
    std::cin >> y;
    switch (y){
        case 1: {

            std::cout << "Drevo ma cenu " << sklad->getCenuDreva() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetDreva()) {
                std::cout << "Nemas dostatok dreva." << std::endl;
                vybermoznostiProdeje();
            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenuDreva()));
                sklad->odoberDrevo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiProdeje();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 2: {
            std::cout << "Kamen ma cenu " << sklad->getCenuKamena() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetKamena()) {
                std::cout << "Nemas dostatok kamena." << std::endl;
                vybermoznostiProdeje();
            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenuKamena()));
                sklad->odoberKamen(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiProdeje();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 3: {
            std::cout << "Zelezo ma cenu " << sklad->getCenuZeleza() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetZeleza()) {
                std::cout << "Nemas dostatok zeleza." << std::endl;
                vybermoznostiProdeje();
            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenuZeleza()));
                sklad->odoberZelezo(mnozstvo);
            }
            if (mnozstvo = 0) {
                vybermoznostiProdeje();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 4: {
            prubehHry();
        }

    }
}
void Hra::prodejVyrobku() {
    std::cout << "Pro prodej zidli zadej 1." << std::endl;
    std::cout << "Pro prodej stolu zadej 2." << std::endl;
    std::cout << "Pro prodej polic zadej 3." << std::endl;
    std::cout << "Pro prodej skrin zadej 4." << std::endl;
    std::cout << "Pro navrat zadej 5." << std::endl;
    int f;
    std::cin >> f;
    switch (f) {
        case 1: {
            std::cout << "Zidle ma cenu: " << sklad->getCenaZidle() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetZidli()) {
                std::cout << "Nemas dostatok zidli." << std::endl;
                prodejVyrobku();

            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenaZidle()));
                sklad->odoberVyrobokZidle(mnozstvo);
            }
            if (mnozstvo = 0) {
                prodejVyrobku();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 2:{
            std::cout << "Stul ma cenu: " << sklad->getCenaStolu() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetStolu()) {
                std::cout << "Nemas dostatok stolu." << std::endl;
                prodejVyrobku();

            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenaStolu()));
                sklad->odoberVyrobokStul(mnozstvo);
            }
            if (mnozstvo = 0) {
                prodejVyrobku();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 3: {
            std::cout << "Police ma cenu: " << sklad->getCenaPolice() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetPolic()) {
                std::cout << "Nemas dostatok polic." << std::endl;
                prodejVyrobku();

            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenaPolice()));
                sklad->odoberVyrobokPolice(mnozstvo);
            }
            if (mnozstvo = 0) {
                prodejVyrobku();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 4: {
            std::cout << "Skrin ma cenu: " << sklad->getCenaSkrine() << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces predat: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo > sklad->getPocetSkrin()) {
                std::cout << "Nemas dostatok skrin." << std::endl;
                prodejVyrobku();

            } else {
                obchod->setFinance(obchod->getFinance() + (mnozstvo * sklad->getCenaSkrine()));
                sklad->odoberVyrobokSkrin(mnozstvo);
            }
            if (mnozstvo = 0) {
                prodejVyrobku();
            }
            obchod->printInfo();
            sklad->printInfo();
            prubehHry();
        }
        case 5:{
            prubehHry();
        }
    }
}
void Hra::vyrobaVyrobku(){
    std::cout << "Pro vyrobu zidli zadej 1." << std::endl;
    std::cout << "Pro vyrobu stolu zadej 2." << std::endl;
    std::cout << "Pro vyrobu polic zadej 3." << std::endl;
    std::cout << "Pro vyrobu skrin zadej 4." << std::endl;
    std::cout << "Pro navrat zadej 5." << std::endl;
    int l;
    std::cin>>l;
    switch (l){
        case 1:{
            std::cout<<"Pro vyrobu zidli potrebujes: " <<std::endl;
            std::cout<< sklad->getCenaDrevaZidle() << " dreva" << std::endl;
            std::cout<< sklad->getCenaKameneZidle() << " kamene" << std::endl;
            std::cout<< sklad->getCenaZelezaZidle() << " zeleza" << std::endl;
            std::cout<< sklad->getCasVyrobyZidle()<< " strojoveho casu" << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces vyrobit: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo==0){
                prubehHry();
            }
            else{
                sklad->pridajVyrobokZidle(mnozstvo);
                obchod->printInfo();
                sklad->printInfo();
                prubehHry();
            }
        }

        case 2:{
            std::cout<<"Pro vyrobu stolu potrebujes: " <<std::endl;
            std::cout<< sklad->getCenaDrevaStul() << " dreva" << std::endl;
            std::cout<< sklad->getCenaKameneStul() << " kamene" << std::endl;
            std::cout<< sklad->getCenaZelezaStul() << " zeleza" << std::endl;
            std::cout<< sklad->getCasVyrobyStul()<< " strojoveho casu" << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces vyrobit: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo==0){
                prubehHry();
            }
            else{
                sklad->pridajVyrobokStul(mnozstvo);
                obchod->printInfo();
                sklad->printInfo();
                prubehHry();
            }
        }
        case 3:{
            std::cout<<"Pro vyrobu police potrebujes: " <<std::endl;
            std::cout<< sklad->getCenaDrevaPolice() << " dreva" << std::endl;
            std::cout<< sklad->getCenaKamenePolice() << " kamene" << std::endl;
            std::cout<< sklad->getCenaZelezaPolice() << " zeleza" << std::endl;
            std::cout<< sklad->getCasVyrobyPolice()<< " strojoveho casu" << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces vyrobit: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo==0){
                prubehHry();
            }
            else{
                sklad->pridajVyrobokPolice(mnozstvo);
                obchod->printInfo();
                sklad->printInfo();
                prubehHry();
            }
        }
        case 4:{
            std::cout<<"Pro vyrobu zidli potrebujes: " <<std::endl;
            std::cout<< sklad->getCenaDrevaSkrin() << " dreva" << std::endl;
            std::cout<< sklad->getCenaKameneSkrin() << " kamene" << std::endl;
            std::cout<< sklad->getCenaZelezaSkrin() << " zeleza" << std::endl;
            std::cout<< sklad->getCasVyrobySkrin()<< " strojoveho casu" << std::endl;
            std::cout << "Zadaj mnozstvo, ktore chces vyrobit: " << std::endl;
            std::cout << "Ak sa chces vratit zadaj: 0 " << std::endl;
            std::cin >> mnozstvo;
            if (mnozstvo==0){
                prubehHry();
            }
            else{
                sklad->pridajVyrobokSkrin(mnozstvo);
                obchod->printInfo();
                sklad->printInfo();
                prubehHry();
            }
        }
    }
}