//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_OBCHOD_H
#define UNTITLED9_OBCHOD_H

#include <iostream>

class Obchod {
    int m_finance;
public:
    Obchod();
    int getFinance();
    void setFinance(int finance);
    void printInfo();
};

#endif //UNTITLED9_OBCHOD_H
