//
// Created by david on 17. 12. 2019.
//

#ifndef UNTITLED9_SURIVONA_H
#define UNTITLED9_SURIVONA_H

#include <iostream>

class Surivona {
std::string m_nazev;
int m_cena;
int m_cenaUskladnenia;
int m_pocet;
public:
    Surivona(std::string nazev,int cena, int cenaUskladnenia);
    std::string getNazev();
    int getCena();
    int getcenaUskladnenia();
    int getPocet();
    void setCena(int cena);
    void setPocet(int pocet);
};


#endif //UNTITLED9_SURIVONA_H
